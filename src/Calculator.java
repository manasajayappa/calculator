
import java.util.Scanner;

public class Calculator 
{

    public static void main(String[] args) 
    {

    	double num1, num2;
        Scanner MyCalculator = new Scanner(System.in); 

        System.out.print("Enter first number:");
        num1 = MyCalculator.nextDouble();
        System.out.print("Enter second number:");
        num2 = MyCalculator.nextDouble(); 

        System.out.print("Enter an operator (+, -, *, /): ");
        char operator = MyCalculator.next().charAt(0);

     

        MyCalculator.close();
        double output;

        switch(operator)
        {
            case '+':
            	output = num1 + num2;
                break;

            case '-':
            	output = num1 - num2;
                break;

            case '*':
            	output = num1 * num2;
                break;

            case '/':
            	output = num1 / num2;
                break;

            case '%':
                 output = num1 % num2;
                 break;    

            default:
                System.out.printf("You have entered wrong operator");
                return;
        }

        System.out.println(num1+" "+operator+" "+num2+": "+output);
    }
}
